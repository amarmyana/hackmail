<?php
include "IMail.php";
class Operations {
	private $imail; 	// imap connection object
	private $mailboxPath;
	public static $mail_to;
	public static $subject;
	public static $message;
	
	/**
	 * Displays percent completed during an operation.
	 */
	
	public function displayPercent($number = "", $total = "") {
		if($number == "") die("ERROR ( OPERATIONS::displayPercent ) :: NO NUMBER SUPPLIED.");
		if($total == "") die("ERROR ( OPERATIONS::displayPercent ) :: TOTAL CANNOT BE LEFT BLANK.");
		$percent = (int) ( $number * 100 ) / $total;
		$percent = number_format($percent);
		echo "\033[18D";
		echo "Progress :   ";
		$output = str_pad($percent, 3, ' ', STR_PAD_LEFT) . " %";    // Output is always 5 characters long
		
		return $output;
	}

	/**
	 * Takes mail provider names as parameters and returns the mailbox path.
	 * Currently supports only gmail and yahoo.
	 * More can be added.
	 */

	public function providerToPath($provider_name = "") {
		if($provider_name == "") {
			die("ERROR ( OPERATIONS::getMailBox ) :: NO NAME SUPPLIED." .
					" PLEASE SUPPLY AN EMAIL PROVIDER'S NAME (eg:- Yahoo, Google).");
		}
		
		$provider_name = strtolower(trim($provider_name));

		switch($provider_name) {
			case "yahoo" :
				return "{imap.mail.yahoo.com:993/imap/ssl}";
			case "gmail" :
				return "{imap.gmail.com:993/imap/ssl}";
			default :
				die("ERROR:- ONLY GMAIL AND YAHOO CURRENTLY SUPPORTED.");
		}
	}

	/**
	 * Returns all mailbox folders in a table with no of msgs they contain.
	 */

	public function displayFolderList($imail = "") {
		if($imail == "") die("ERROR ( OPERATIONS::displayFolderList ) :: NO IMAIL OBJECT SUPPLIED.");

		$con = $imail->mailConnect();
		$list = $imail->getFolderList($con);
		$info = $imail->getInfo($con);
		$longestNumMsg = strlen($info->Nmsgs) + 2;
		$list = $imail->cleanFolderList($list);
		$longestName = max(array_map('strlen', $list)) + 7;
		
		$repeat = $longestName + $longestNumMsg + 5;
		
		$output = "\n" . str_repeat("=", $repeat) . "\n";				//First line of the table
		
		$output .= "|" . str_pad("Folders", $longestName + $longestNumMsg + 3, " ", STR_PAD_BOTH) . "|\n"; //Table header
		
		$i = 1;
		$total = count($list);

		$output .= str_repeat("=", $repeat) . "\n"; 			//Third line of the table
		foreach ($list as $folder) {

			echo $this->displayPercent($i, $total);				//Display the progress in %
			$imail->setFolder($folder);
			$con = $imail->mailConnect();
			$number_of_messages = $imail->getNumMsg($con);
			$folder = $imail->cleanFolder($folder);
			$folder = "\033[31m" . $folder  . "\033[0m";  // \033[31m is ascii for the colour red.
			//$folder .= "   " . $number_of_messages;
			$output .= "|" . str_pad(" " . $folder, $longestName + 9, " ", STR_PAD_RIGHT) . "|" . // <== Constructs the table
					str_pad(" " . $number_of_messages . " ", $longestNumMsg + 2, " ", STR_PAD_BOTH) . "|\n";
			$i++;
		}
		$output .= str_repeat("=", $repeat) . "\n"; //Last line of the table

		$imail->mailDisconnect($con);
		
		return $output;
	}
	
	/**
	 * 
	 * object $imail
	 * string $folder
	 * Returns headers specified in the range.
	 * Returns all headers if $fromNumber declared as all.
	 * Another optional parameter "DESC" can be passed (only when $fromNumber = ALL) to $toNumber to
	 * display all headers in reverse order.
	 */
	
	public function getMailList($imail = "", $folder = "", $fromNumber = "", $toNumber = "") {
		if($imail == "") die("ERROR ( OPERATIONS::getMailList ) :: NO IMAIL OBJECT SUPPLIED.");
		
		if($folder != "") {
			$imail->setFolder($folder);
		}

		echo "Connecting to your mailbox...\n";
		$con = $imail->mailConnect();
		echo "Done...\n";
		$numOfMsgs = $imail->getNumMsg($con);

		$lineLength = 80;
		$line = str_repeat("+", $lineLength);
		$redLine = "\033[31m" . $line . "\033[0m \n";
		$blueLine = "\033[34m" . $line . "\033[0m \n";
		$output = "\n";


		if($fromNumber == "" && $toNumber == "") {
			die("ERROR ( OPERATIONS::getMailList ) :: RANGE CANNOT BE LEFT BLANK. " .
					"TYPE 'ALL' TO DISPLAY ALL TO DISPLAY ALL ALONG WITH ORDER ( 'ASC / DESC' )");
		} else if(strtolower($fromNumber) == "all") {

			if(strtolower($toNumber) == "desc") {
				
				for($i = 1; $i <= $numOfMsgs; $i++) {
					
					echo $this->displayPercent($i, $numOfMsgs);
					$header  = $imail->getHeader($con, $i);

					$msgNo   = $header->Msgno;
					$unSeen  = ($header->Unseen == "U") ?  "UNREAD" : "READ";
					$date    = $header->date;
					$subject = $header->subject;
					$from    = $header->fromaddress;

					if(strlen($subject) > 67) $subject = substr($subject, 0, 64) . "...";  	// If subject is longer than the table
														// Shorten the sentance and add "..."
					if(strlen($from) > 67) $from = substr($from, 0, 64) . "...";

					$row1 = " Date :- " . $date . "   |  Msg No:- " . $msgNo . "  |  " . $unSeen;
					$row2 = " From :- " . $from;
					$row3 = "Subject :- " . $subject;


					$output .= "\n" . $redLine;
					$output .= $blueLine;
					$output .= "|" . str_pad(" " . $row1, ($lineLength - 2), " ", STR_PAD_RIGHT) . "|\n";
			
					$output .= $blueLine;
					$output .= "|" . str_pad(" " . $row2, ($lineLength - 2), " ", STR_PAD_RIGHT) . "|\n";

					$output .= $blueLine;
					$output .= "|" . str_pad($row3, ($lineLength - 2), " ", STR_PAD_BOTH) . "|\n";

					$output .= $blueLine;
					$output .= $redLine;
				}
			} else {
				$total = $numOfMsgs;
				for($numOfMsgs, $i = 1; 1 <= $numOfMsgs; $numOfMsgs--, $i++) {
					
					echo $this->displayPercent($i, $total);	
					$header = $imail->getHeader($con, $numOfMsgs);
						
					$msgNo   = $header->Msgno;
					$unSeen  = ($header->Unseen == "U") ?  "UNREAD" : "READ";
					$date    = $header->date;
					$subject = $header->subject;
					$from    = $header->fromaddress;

					if(strlen($subject) > 67) $subject = substr($subject, 0, 64) . "...";  	// If subject is longer than the table
					if(strlen($from) > 67) $from = substr($from, 0, 64) . "...";		// Shorten the sentance and add "..."


					$row1 = " Date :- " . $date . "   |  Msg No:- " . $msgNo . "  |  " . $unSeen;
					$row2 = " From :- " . $from;
					$row3 = "Subject :- " . $subject;



					$output .= "\n" . $redLine;
					$output .= $blueLine;
					$output .= "|" . str_pad(" " . $row1, ($lineLength - 2), " ", STR_PAD_RIGHT) . "|\n";
			
					$output .= $blueLine;
					$output .= "|" . str_pad(" " . $row2, ($lineLength - 2), " ", STR_PAD_RIGHT) . "|\n";

					$output .= $blueLine;
					$output .= "|" . str_pad($row3, ($lineLength - 2), " ", STR_PAD_BOTH) . "|\n";

					$output .= $blueLine;
					$output .= $redLine;	
				}
			}
		} else {
			$total = ($toNumber + 1) - $fromNumber;
			if($fromNumber > $toNumber) die("ERROR ( OPERATIONS::getMailList ) ::" .
									" FROM-NUMBER (" . $fromNumber . ") CANNOT BE LAGER THAN TO-NUMBER(" . $toNumber . ").");
			for($fromNumber, $i = 1; $fromNumber <= $toNumber;  $fromNumber++, $i++){
				
				echo $this->displayPercent($i, $total);
				
				$header = $imail->getHeader($con, $fromNumber);
				

				$msgNo   = $header->Msgno;
				$unSeen  = ($header->Unseen == "U") ?  "UNREAD" : "READ";
				$date    = $header->date;
				$subject = $header->subject;
				$from    = $header->fromaddress;

				if(strlen($subject) > 67) $subject = substr($subject, 0, 64) . "...";  	// If subject is longer than the table
													// Shorten the sentance and add "..."

				if(strlen($from) > 67) $from = substr($from, 0, 64) . "...";


				$row1 = " Date :- " . $date . "   |  Msg No:- " . $msgNo . "  |  " . $unSeen;
				$row2 = " From :- " . $from;
				$row3 = "Subject :- " . $subject;



				$output .= "\n" . $redLine;
				$output .= $blueLine;
				$output .= "|" . str_pad(" " . $row1, ($lineLength - 2), " ", STR_PAD_RIGHT) . "|\n";
		
				$output .= $blueLine;
				$output .= "|" . str_pad(" " . $row2, ($lineLength - 2), " ", STR_PAD_RIGHT) . "|\n";

				$output .= $blueLine;
				$output .= "|" . str_pad($row3, ($lineLength - 2), " ", STR_PAD_BOTH) . "|\n";

				$output .= $blueLine;
				$output .= $redLine;
			}
		}

		$imail->mailDisconnect($con);

		return $output;
	}

	public function displayMessage($imail = "", $msgNo = "") {
		if($imail == "") die("ERROR ( OPERATIONS::displayMessages ) :: NO IMAIL OBJECT SUPPLIED.");
		if($msgNo == "") die("ERROR ( OPERATIONS::displayMessages ) :: NO MESSAGE NUMBER SUPPLIED.");
		
		$con = $imail->mailConnect();
		$data = $imail->getMessage($con, $msgNo);

		$lineLength = 100;
		$line	    = str_repeat("+", $lineLength);
		$innerLine  = str_repeat("+", ($lineLength - 2));
		$redLine    = "\033[31m" . $line . "\033[0m \n";
		$blueLine   = "\033[34m" . $innerLine . "\033[0m";


		if(is_array($data)) {
			$rep	     = array("\t", "\n", "\r", "  ");	// Replace these characters in the message so that they don't cause any problems.
			$message     = $data['message'];		// Exatract message from the array and delete the key.
			unset($data['message']);

			$message     = str_replace($rep, "", $message);	// Clean message.
			$message     = chunk_split($message, 96, "\n");
			$message     = explode("\n", $message);		// Split up messsage.


			$lastElement = count($data);			// Total amount of elements in the array is equal to the last element.

			$i = 1;						// Temp variables
			$j = 1;
			$punch = ",";

			$output  = $redLine;
			$output .= "|" . $blueLine . "|\n";
			$output .= "|" . str_pad(" Attachments (" . $lastElement . ") :- ", 98, " ", STR_PAD_RIGHT) . "|\n";
			$output .= "|" . $blueLine . "|\n";
			$output .= "|";
			foreach($data as $attach) {
				if(($strlen = strlen($attach)) > 32) {
					$from   = 20;
					$length = $strlen - 24;
					$attach = substr_replace($attach, "...", $from, $length);

				}

				if($i == $lastElement) {
					$punch = ".";
					if($j == 1) {
						$output .= str_pad(" " . $attach . $punch, 97, " ", STR_PAD_RIGHT) . " |\n";
					} else if($j == 2) {
						$output .= str_pad(" " . $attach . $punch, 65, " ", STR_PAD_RIGHT) . "|\n";
					} else {
						$output .= str_pad(" " . $attach . $punch, 34, " ", STR_PAD_RIGHT) . "|\n";
					}
				} else {
					$attach = str_pad(" " . $attach . $punch, 32, " ", STR_PAD_RIGHT);

					if(($i % 3) == 0) {
						$output .= $attach . "  |\n|";
						$j = 1;
					} else {
						$output .= $attach;
						$j++;
					}
				}
				$i++;
			}
			$output .= "|" . $blueLine . "|\n";
			$output .= "|" . str_pad(" Message :- ", 98, " ", STR_PAD_RIGHT) . "|\n";
			$output .= "|" . $blueLine . "|\n";
			foreach($message as $eachLine) {
				$output .= "| " . str_pad($eachLine, 97, " ", STR_PAD_RIGHT) . "|\n";
			}
			$output .= "|" . $blueLine . "|\n";
			$output .= $redLine . "\n";
		} else {
			$rep	     = array("\n", "\r", "  ");	// Replace these characters in the message so that they don't cause any problems.

			$data     = str_replace($rep, "", $data);	// Clean message.
			$data     = chunk_split($data, 96, "\n");
			$data     = explode("\n", $data);		// Split up messsage.

			$output  = $redLine;
			$output .= "|" . $blueLine . "|\n";
			$output .= "|" . str_pad(" Messsage :- ", 98, " ", STR_PAD_RIGHT) . "|\n";
			$output .= "|" . $blueLine . "|\n";
			foreach($data as $eachLine) {
				$output .= "| " . str_pad($eachLine, 97, " ", STR_PAD_RIGHT) . "|\n";
			}
			$output .= "|" . $blueLine . "|\n";
			$output .= $redLine;
		}

		$imail->mailDisconnect($con);

		return $output;
	}

	public function markForDelete($imail, $messageNumber) {
		if($imail == "") die("ERROR ( OPERATIONS::deleteMilboxMessage ) :: NO IMAIL OBJECT SUPPLIED.");
		if($messageNumber == "") die("ERROR ( OPERATIONS::deleteMailboxMessage ) :: NO MESSAGE NUMBER SUPPLIED.");
		$con = $imail->mailConnect();
		$imail->deleteMessage($con, $messageNumber);
	}

	public function unMark($imail, $messageNumber) {
		if($imail == "") die("ERROR ( OPERATIONS::unMark ) :: NO IMAIL OBJECT SUPPLIED.");
		if($messageNumber == "") die("ERROR ( OPERATIONS::unMark ) :: NO MESSAGE NUMBER SUPPLIED.");
		$con = $imail->mailConnect();
		$imail->unDeleteMessage($con, $messageNumber);
	}

	public function hardDelete($imail) {
		if($imail == "") die("ERROR ( OPERATIONS::hardDelete ) :: NO IMAIL OBJECT SUPPLIED.");
		$con = $imail->mailConnect();
		$imail->expungeMessage($con);
	}

	public function checkValidFolder($imail = "") {
		if($imail == "") die("ERROR ( OPERATIONS::checkValidFolder ) :: NO IMAIL OBJECT SUPPLIED.");
		@$con = $imail->mailConnect();
		if($imail->getNumMsg($con) != 0) {
			return TRUE;
		}
	}

	
	public function downloadAllAttachments($imail = "", $messageNumber = "", $path = "") {
		if($imail == "")	 die("ERROR ( OPERATIONS::downlodAllAttachments ) :: NO IMAIL OBJECT SUPPLIED.");
		if($messageNumber == "") die("ERROR ( OPERATIONS::dowloadAllAttchments ) :: NO MESSAGE NUMBER SUPPLIED.");

		$con 	   = $imail->mailConnect();
		$msgID	   = $imail->getMsgID($con, $messageNumber);
		$structure = $imail->fetchStructure($con, $msgID);

		return $imail->downloadAttachments($con, $messageNumber, $structure, $path);
	}

	public function createNew() {

	}

	public function sendEmail($imail = "", $to = "", $subject = "", $message = "", $headers = "") {
		if($imail == "") die("ERROR ( OPERATIONS::sendEmail ) :: NO IMAIL OBJECT SUPPLIED.");
		if($to == "")	 die("ERROR ( OPERATIONS::sendEmail ) :: NO RECIPIENTS SUPPLIED.");
		if($imail->sendMail($to, $subject, $message, $headers)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>
