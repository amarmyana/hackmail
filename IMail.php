<?php
/**
 * A simple implementation of the php imap library.
 * ________________________________________________
 *
 *        Author - Jonathan Fernandes
 *        Email - junkie.jonny@gmail.com
 */

class IMail {

	private $username;
	private $password;
	private $mailboxPath;
	private $folder;
	private $smtpHost;
	private $smtpPort;

	public function __construct($username = "", $password = "", $mailboxPath = "", $folder = "INBOX") {
		if($username == "")    die("ERROR ( IMAIL::__construct ) :: NO USERNAME SUPPLIED.");
		if($password == "")    die("ERROR ( IMAIL::__construct ) :: NO PASSWORD SUPPLIED.");
		if($mailboxPath == "") die("ERROR ( IMAIL::__construct ) :: NO MAILBOXPATH SUPPLIED.");
		
		$this->username    = $username;
		$this->password    = $password;
		$this->mailboxPath = $mailboxPath;
		$this->folder 	   = $folder;
	}

	public function setUserName($username = "") {
		if($user == "") die("ERROR ( IMAIL::setUserName ) :: NO USERNAME GIVEN.");

		$this->username = $username;
	}

	public function getUserName() {
		return $this->username;
	}

	public function setPassword($password = "") {
		if($password == "") die("ERROR ( IMAIL::setPassword ) :: NO PASSWORD GIVEN.");

		$this->password = $password;
	}

	private function getPassword() {   //<==  DONT KNOW IF THIS IS NEEDED.
		return $this->password;
	}

	public function setMailboxPath($mailboxPath = "") {
		if($mailboxPath == "") die("ERROR ( IMAIL::setMailboxPath ) :: NO MAILBOX PATH GIVEN.");

		$this->mailboxPath = $mailboxPath;
	}

	public function getMailboxPath() {
		return $this->mailboxPath . $this->getFolder();
	}


	public function setFolder($folder = "") {
		if($folder == "") die("ERROR ( IMAIL::setFolder ) :: NO FOLDER SUPPLIED.");
		$this->folder = $folder;
	}

	public function getFolder() {
		return $this->folder;
	}

	public function setSmtpHost($hostName = "") {
		if($hostName == "") die("ERROR ( IMAIL::setSmtpHost ) :: NO HOSTNAME SUPPLIED.");
		$this->smtpHost = $hostName;
	}

	public function getSmtpHost() {
		return $this->smtpHost;
	}

	public function setSmtpPort($portNumber = "") {
		if($portNumber == "") die("ERROR ( IMAIL::setSmtpPort ) :: NO PORT NUMBER SUPPLIED.");
		$this->smtpPort = $portNumber;
	}

	public function getSmtpPort() {
		return $this->smtpPort;
	}

	public function mailConnect() {
		try {
			$con = imap_open($this->getMailboxPath(),
				$this->getUserName(),
				$this->getPassword());
		} catch(Exception $e) {
			echo "COULD NOT ESTABLISH A CONNECTION. " .
				"( CHECK USERNAME / PASSWORD / MAILBOXPATH )";
		}

		return $con;
	}

	public function mailDisconnect($con = "") {
		if($con == "") die("ERROR (IMAIL::mailDisconnect) :: NO IMAP LINK SUPPLIED.");
		imap_close($con);
	}
	
	public function getInfo($con = "") {
		if($con == "") die("ERROR (IMAIL::getInfo) :: NO RESOURCE SUPPLIED.");
		return imap_check($con);
	}

	public function getFolderList($con = "", $expression = "*") {
		if($con == "") die("ERROR ( IMAIL::getFolderList ) :: NO RESOURCE SUPPLIED.");
		return imap_list($con, $this->mailboxPath, $expression);
	}
	
	public function cleanFolderList($list = "") {
		if($list == "") die("ERROR ( IMAIL::cleanFolderList ) :: NO LIST SUPPLIED.");
		foreach($list as $folder) {
			$folders[] = str_replace($this->mailboxPath, "", imap_utf7_decode($folder)); // changed form get to variablename.
		}
		return $folders;
	}
	
	public function cleanFolder($folder = "") {
		if($folder == "") die("ERROR ( IMAIL :: cleanFolder ) :: NO FOLDER SUPPLIED.");
		return str_replace($this->getMailboxPath(), "", imap_utf7_decode($folder));
	}

	public function getNumMsg($con = "") {
		if($con == "") die("ERROR ( IMAIL::getNumMsg ) :: NO RESOURCE SUPPLIED.");
		return imap_num_msg($con);
	}

	/**
	 * Returns header of only one mail.
	 */
	public function getHeader($con = "", $msg_no = "") {
		if($con == "")	  die("ERROR ( IMAIL::getHeader ) :: NO RESOURCE SUPPLIED.");
		if($msg_no == "") die("ERROR ( IMAIL::getHeader ) :: NO MESSAGE NUMBER SUPPLIED.");
		return imap_header($con, $msg_no);
	}
	
	/**
	 * Returns all headers of a mailbox.
	 */
	
	public function getHeaders($con = "") {
		if($con == "") die("Error ( IMAIL::getHeaders ) :: NO RESOURCE SUPPLIED.");
		return imap_headers($con);
	}

	public function getMsgID($con = "", $msg_no = "") {
		if($con == "")	  die("ERROR ( IMAIL::setMsgID ) :: NO RESOURCE SUPPLIED.");
		if($msg_no == "") die("ERROR ( IMAIL::setMsgID ) :: NO MESSAGE NUMBER SUPPLIED.");
		return imap_uid($con, $msg_no);
	}

	public function fetchStructure($con = "", $msg_id = "") {
		if($con == "")	  die("ERROR ( IMAIL::fetchStructure ) :: NO RESOURCE SUPPLIED.");
		if($msg_id == "") die("ERROR ( IMAIL::fetchStructure ) :: NO MESSAGE NUMBER SUPPLIED.");
		return imap_fetchstructure($con, $msg_id, FT_UID);
	}

	public function getMimeType($structure = "") {
		if($structure == "") die("ERROR ( IMAIL::getMimeType ) :: NO STRUCTURE OBJECT SUPPLIED.");
		$tmpMimeType = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

		if($structure->subtype) {
			return $tmpMimeType[(int) $structure->type] . "/" . $structure->subtype;
		}

		return "TEXT/PLAIN";
	}

	public function fetchBody($con = "", $msg_no = "", $partNumber = 0) {
		if($con == "")	   die("ERROR ( IMAIL::fetchBody ) :: NO RESOURCE SUPPLIED.");
		if($msg_no == "")  die("ERROR ( IMAIL::fetchBody ) :: NO MESSAGE NUMBER SUPPLIED.");
		if($partNumber == 0) {
			return imap_fetchbody($con, $msg_no, FT_UID);
		} else {
			return imap_fetchbody($con, $msg_no, $partNumber, FT_UID);
		}
	}

	public function decodeMessage($encoding, $message) {
		switch($encoding) {
			case 0 :
				//return $message;                  <== Uncomment this later!...
			case 1 :
				return imap_8bit($message);
			case 2 :
				return imap_binary($message);
			case 3 :
				return imap_base64($message);
			case 4 :
				return imap_qprint($message);
			default :
				return $message;
		}
	}

	public function parsePart($encoding, $mime, $message) {
		if($mime == "TEXT/HTML") {
			$rep	 = array("&nbsp;", "  ", "\t", "\n\r");
			$message = strip_tags($message);	
			$message = $this->decodeMessage($encoding, $message);
			$message = str_replace($rep, "", $message);
		} else {
			$message = $this->decodeMessage($encoding, $message);
		}
		
		$message = wordwrap($message, 80, "\n");

		return $message;
	}

	/**
	 * Gets The message part of an email.
	 */

	public function getMessage($con, $msgNumber) {
		if($con == "")	     die("ERROR ( IMAIL::getMessage ) :: NO RESOURCE SUPPLIED.");
		if($msgNumber == "") die("ERROR ( IMAIL::getMessage ) :: NO MESSAGE NUMBER SUPPLIED.");

		$msgID 	   = $this->getMsgID($con, $msgNumber);
		$structure = $this->fetchStructure($con, $msgID);
		if(isset($structure->parts)) {
			$parts = $structure->parts;
			$newStructure = $parts[0];
			$partCount    = count($parts); 
			if($partCount >= 2) {
				if(isset($newStructure->parts)) {
					$msgStructure	   = $newStructure->parts[0];
					$encoding	   = $msgStructure->encoding;
					$mime		   = strtoupper(trim($this->getMimeType($msgStructure)));
					$message	   = $this->fetchBody($con, $msgID, 1.1);
					$output['message'] = $this->parsePart($encoding, $mime, $message);

					//Gets Attachment names

					foreach($parts as $key => $parts) {
						if($key == 0) continue;
						$name = "attachment_" . $key;
						$output[$name] = $parts->dparameters[0]->value;
					}
				} else {
					$encoding = $newStructure->encoding;
					$mime	  = strtoupper(trim($this->getMimeType($newStructure)));
					$message  = $this->fetchBody($con, $msgID, 1);
					$output	  = $this->parsePart($encoding, $mime, $message);
				}
			}
		} else {
			$encoding = $structure->encoding;
			$mime	  = strtoupper(trim($this->getMimeType($structure)));
			$message  = $this->fetchBody($con, $msgNumber, 0);
			$output   = $this->parsePart($encoding, $mime, $message);
		}
		return $output;
	}

	public function downloadAttachments($con = "", $msgNumber = "", $structure = "", $path = "") {
		if($con == "") 	     die("ERROR ( IMAIL::downloadAttachments ) :: NO RESOURCE SUPPLIED.");
		if($msgNumber == "") die("ERROR ( IMAIL::downloadAttachments ) :: NO MESSAGE NUMBER SUPPLIED.");
		if($structure == "") die("ERROR ( IMAIL::downloadAttachments ) :: NO STRUCTURE SUPPLIED.");
		if($path == "") $path = "/home/" . get_current_user() . "/Downloads/";

		$msgID = $this->getMsgID($con, $msgNumber);
		$parts = $structure->parts;

		foreach($parts as $num => $part) {
			if($num == 0) continue;
			$fileName = $part->dparameters[0]->value;
			echo "Downloading file - " . $fileName . "\n";
			echo "Mime - " . $mime	  = strtoupper(trim($this->getMimeType($part))) . "\n";
			echo "Encoding - " . $encoding = $part->encoding . "\n";
			$data	  = $this->fetchBody($con, $msgID, ($num + 1));
			$data	  = $this->decodeMessage($encoding, $data);
			//$data	  = $this->parsePart($encoding, $mime, $data);
			echo "Saving file to - " . $path . "\n";
			$file = $path . $fileName;
			$fh = fopen($file, "w");
			if(fwrite($fh, $data) === FALSE) {
				return false;
			} else {
				echo "Done\n";
			}
			fflush($fh);
			fclose($fh);
		}
		return TRUE;
	}

	public function deleteMessage($con = "", $messageNumber = "") {	//Note :- This will only mark a message for deletion.
		if($con =="")	 	 die("ERROR ( IMAIL::deleteMessage ) :: NO RESOURCE SUPPLIED.");
		if($messageNumber == "") die("ERROR ( IMAIL::deleteMessage ) :: NO MESSAGE NUMBER SUPPLIED.");
		$msgID = $this->getMsgID($con, $messageNumber);
		imap_delete($con, $msgID, FT_UID);
	}

	public function unDeleteMessage($con = "", $messageNumber = "") {
		if($con == "")		 die("ERROR ( IMAIL::unDeleteMessage ) :: NO RESOURCE SUPPLIED.");
		if($messageNumber == "") die("ERROR ( IMAIL::unDeleteMessage ) :: NO RESOURCE SUPPLIED.");
		$msgID = $this->getMsgID($con, $messageNumber);
		imap_undelete($con, $msgID, FT_UID);
	}

	public function expungeMessage($con = "", $messageNumber = "") {
		if($con == "") die("ERROR ( IMAIL::expungeMessage ) :: NO RESOURCE SUPPLIED.");
		imap_expunge($con);
	}

	public function pingMailbox($con = "") {
		if($con == "") die("ERROR ( IMAIL::pingMailBox ) :: NO RESOURCE SUPPLIED.");
		return imap_ping($con);
	}

	/**
	 * Creates a socket to the mailbox.
	 */

	public function createSocket($timeout = "") {
		$host = $this->getSmtpHost();
		$port = $this->getSmtpPort();

		$socket = fsockopen($host, $port, $errno, $errstr, 15);
		if($socket !== FALSE) {
			return $socket;
		} else {
			return FALSE;
		}
	}

	public function destroySocket($socket = "") {
		if($socket == "") die("ERROR ( IMAIL::destroySocket ) :: NO SOCKET HANDLER GIVEN.");
		fclose($socket);
	}

	public function serverParse($socket, $expectedResponse) {
		$serverResponse = "";
		while(substr($serverResponse, 3, 1) != ' ') {
			if(!($serverResponse = fgets($socket, 256))) {
				die("Couldn't get mail server response codes." .
					" Please report this bug at int3rlop3r@yahoo.in.",__FILE__, __LINE__);
			}
		}

		if(!(substr($serverResponse, 0, 3) == $expectedResponse)) {
			die("Unable to send e-mail. Please report this bug at int3rlop3r@yahoo.in with the following message - " .
				$serverResponse ." ", __FILE__, __LINE__);
		}
	}

	public function writeToSocket($socket, $data) {
		if($socket == "") die("ERROR ( IMAIL::writeToSocket ) :: NO SOCKET HANDLER SUPPLIED.");
		if($data == "")	  die("ERROR ( IMAIL::writeToSocket ) :: NO DATA SUPPLIED.");
		fwrite($socket, $data);
	}

	/**
	 * This function sends an mail. This whole idea of sending the email was originally a part of the forum software phpBB (http://www.phpbb.com).
	 * I modified it to a great extent, but I wouldn't have been able to do it without them.
	 */

	public function sendMail($to = "", $subject = "", $message = "", $headers = "") {
		if($to == "") die("ERROR ( IMAIL::sendMail ) :: NO RECIPIENTS SUPPLIED.");


		$user	    	 = $this->getUserName();
		$password   	 = $this->getPassword();
		$smtpHost	 = $this->getSmtpHost();
		$smtpPort	 = $this->getSmtpPort();
		$recipients      = explode(",", $to);

		$EHLO		 = "EHLO " . $smtp_host . "\r\n";
		$AUTH_LOGIN	 = "AUTH LOGIN" . "\r\n";
		$USER		 = base64_encode($user) . "\r\n";
		$PASS		 = base64_encode($password) . "\r\n";
		$MAIL_FROM	 = "MAIL FROM: <" . $user . ">";
		$MAIL_TO	 = explode(", ", "RCPT TO: <" . str_replace(", ", "\r\n, RCPT" . $to) . "\r\n"); 	// Expected output - 'RCPT TO: <'.$email.'>'
		$DATA		 = "DATA\r\n";
		$SUBJECT	 = "Subject: " . $subject . "\r\nTo: <" . implode(">, <" . $recipients . ) . ">\r\n" . $headers . "\r\n\r\n" . $message . "\r\n";
		$DOT		 = ".\r\n";
		$QUIT		 = "QUIT\r\n";

		$expectedResults = array(
					'BLANK'      => '220',
					'EHLO'       =>'250',
					'AUTH_LOGIN' => '334',
					'USER'       => '334',
					'PASS'       => '235',
					'MAIL_FROM'  => '250',
					'MAIL_TO'    => '250',
					'DATA'       => '354',
					'SUBJECT'    => '250',
					'QUIT'       => ''
					);

		$i		 = 0;

		$socket = $this->createSocket(15);

		foreach($expectedResults as $key => $result) {
			switch($key) {
				case 'BLANK' :
					$this->serverParse($socket, $result);
					break;
				case 'EHLO' :
					$this->writeToSocket($socket, $EHLO);
					$this->serverParse($socket, $result);
					break;
				case 'AUTH_LOGIN' :
					$this->writeToSocket($socket, $AUTH_LOGIN);
					$this->serverParse($socket, $result);
					break;
				case 'USER' :
					$this->writeToSocket($socket, $USER)
					$this->serverParse($socket, $result);
					break;
				case 'PASS' :
					$this->writeToSocket($socket, $PASS);
					$this->serverParse($socket, $result);
					break;
				case 'MAIL_FROM' :
					$this->writeToSocket($socket, $MAIL_FROM);
					$this->serverParse($socket, $result);
					break;
				case 'MAIL_TO' :
					foreach($MAIL_TO as $mail_to) {
						$this->writeToSocket($socket, $mail_to);
						$this->serverParse($socket, $result);
					}
					break;
				case 'DATA' :
					$this->witeToSocket($socket, $DATA);
					$this->serverParse($socket, $result);
					break;
				case 'SUBJECT' :
					$this->writeToSocket($socket, $SUBJECT);
					$this->writeToSocket($socket, $DOT);
					$this->serverParse($socket, $result);
					break;
				case 'QUIT' :
					$this->destroySocket($socket);
					return TRUE;
				default :
					echo "Invalid operation";
			}
		}

		return FALSE;
	}
}
?>
