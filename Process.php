<?php
include "Operations.php";
include "config.php";


class Process {
	private $imail;
	private $operations;
	private $config;

	public function __construct() {
		$this->config	  = new Config();
		$this->imail	  = new IMail($this->config->username, $this->config->password, $this->config->mailbox);	
		$this->operations = new Operations();
		
		$this->imail->setSmtpHost($this->config->smptHost);
		$this->imail->setSmtpPort($this->config->smtpPort);
	}

	public function getCmd($cmd = "") {
		if($cmd != "") {
			$cmd = trim($cmd, "\n");
			$cmd = trim($cmd);
			if(preg_match("/\s/", $cmd)) {
				$new_cmd   = explode(" ", $cmd);
				$cmd_count = count($new_cmd);
				
				switch($new_cmd[0]) {
					case "select-folder" :
						if($cmd_count == 2) {
							$this->changeMailboxFolder($new_cmd[1]);
						}
						else if($cmd_count > 2) {
							array_shift($new_cmd);
							$file_name = implode(" ", $new_cmd);
							$this->changeMailboxFolder($file_name);
						} else {
							echo "Invalid command!...\n";
						}
						break;
					case "show-all-mails" :
						if($cmd_count == 2) {
							@$this->showMails($new_cmd[1]);
						} else if($cmd_count == 3) {
							@$this->showMails($new_cmd[1], $new_cmd[2]);
						} else {
							echo "Invalid Command!...\n" .
								"Usage :- show-all-mails OR " .
								"show-all-mails <from> <to> OR " .
								"show-all-mails ALL [desc] (Desc is optional)\n";
						}
						break;
					case "show-mail" :
						if($cmd_count == 2 && is_numeric($new_cmd[1])) {
							$this->showMail($new_cmd[1]);
						} else {
							echo "Invalid Command!...\n" .
								"Usage :- show-mail <message-number>\n";
						}
						break;
					case "mark" :
						if($cmd_count == 2 && is_numeric($new_cmd[1])) {
							$this->mark($new_cmd[1]);
						} else {
							echo "Invalid Command!...\n" .
								"Usage :-  mark <message-number>\n";
						}
						break;
					case "unmark" :
						if($cmd_count == 2 && is_numeric($new_cmd[1])) {
							$this->unmark($new_cmd[1]);
						} else {
							echo "Invalid Command!...\n" .
								"Usage :- unmark <message-number>\n";
						}
						break;
					case "download-attc" :
						if($cmd_count <= 3 && is_numeric($new_cmd[1])) {
							if(isset($new_cmd[2]) && $new_cmd[2] != "") {
								$this->downloadAllAttc($new_cmd[1], $new_cmd[2]);
							} else {
								$this->downloadAllAttc($new_cmd[1]);
							}
						} else {
							echo "Invalid Command!...\n" .
								"Usage :- download-attc <message-number> [path]\n";
						}
						break;
					default :
						echo "Invalid Command!...\n";
				}
			} else {
				switch($cmd) {
					case "help" :
						$this->showHelp();
						break;
					case "show-all-folders" :
						$this->showFolders();
						break;
					case "show-all-mails" :
						@$this->showMails("ALL");
						break;
					case "show-folder-name" :
						$this->showFolderName();
						break;
					case "delete" :
						$this->deleteMarkedMails();
						break;
					default :
						echo "Invalid Command!...\n";	
				}
			}
		}
	}

	public function showHelp() {
		$output = <<< _msg_
This is a simple E-mail client that allows you to view your emails in your terminal.

	Usage :-

	help				: prints this message.

	show-all-folders		: displays all the folders in the mailbox.

	show-all-mails			: displays basic info of all mails in a folder.
					  Usage :- show-all-mails OR
	                                  show-all-mails <from> <to> OR
					  show-all-mails ALL [desc] (Desc is optional).

	select-folder			: selects a mailbox folder.
					  Usage :- select-folder <foldername>

	show-mail			: Displays a "message-number'th" email.
					  Usage :- show-mail <message-number>

	show-folder-name		: Displays name of current folder in use.

_msg_;

		echo $output;
	}

	public function showFolders() {
		echo "Folders in your mailbox :- \n\n";
		echo $this->operations->displayFolderList($this->imail);
	}

	public function showMails($from = "", $to = "") {	
		echo "Mails in current mailbox :- \n\n";
		if($from == "ALL") {
			if($to == "desc") {
				echo $this->operations->getMailList($this->imail, "", "ALL", "desc");
			} else {
				echo $this->operations->getMailList($this->imail, "", "ALL");
			}
		} else if(is_numeric($from) && is_numeric($to)) {
			//$from = intval(trim($from));
			//$to = intval(trim($to);
			echo $this->operations->getMailList($this->imail, "", $from, $to);
		} else {
			echo "Invalid data.....\n";
		}
	}

	public function showFolderName() {
		echo $this->imail->getFolder() . "\n";
	}

	public function showMail($msgNo) {
		echo $this->operations->displayMessage($this->imail, $msgNo);
	}

	public function changeMailboxFolder($folderName) {
		try {
			$this->imail->setFolder($folderName);
			if($this->operations->checkValidFolder($this->imail) !== TRUE) {
				echo "Warning :: Folder " . $folderName . " does not exists or contains no mails.\n";
				$folderName = "Inbox";
				$this->imail->setFolder($folderName);
			}
			echo $folderName . " selected!\n";
		} catch(Exception $e) {
			echo "Something went wrong.\nThat folder could not be selected.\n";
		}
	}

	public function mark($msgNumber) {
		$this->operations->markForDelete($this->imail, $msgNumber); // or die("Error deleting msg.");
		echo "Message " . $msgNumber . " marked.\n";
	}

	public function unmark($msgNumber) {
		$this->operations->unMark($this->imail, $msgNumber);
		echo "Message " . $msgNumber . " un-marked.\n";
	}

	public function deleteMarkedMails() {
		$this->operations->hardDelete($this->imail);
		echo "Selected messages have been deleted!\n";
	}

	public function downloadAllAttc($messageNumber = "", $savePath = "") {
		if($this->operations->downloadAllAttachments($this->imail, $messageNumber, $savePath)) {
			echo "All attchments downloaded!...\n";
		}
	}
}
?>
