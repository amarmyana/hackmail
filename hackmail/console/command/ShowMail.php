<?php namespace HackMail\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of ShowMail
 *
 * @author ace
 */

class ShowMail extends Command {
    
    protected function configure()
    {
         
        $this->setName('showMail')
             ->setDescription('Shows the Given Id Mail')
             ->addArgument('id', InputArgument::REQUIRED, 'Specify the id of the Mail?')
             ->setHelp(sprintf(
                '%sShows the Mail with Specified id or by default all Mails%s',
                PHP_EOL,
                PHP_EOL
            ));
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $id = $input->getArgument('id');
        
        $output->writeln("You Have Entered Id :: $id" . PHP_EOL);
    }
}
