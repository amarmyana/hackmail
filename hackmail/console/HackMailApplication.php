<?php namespace HackMail\Console;

/**
 * Description of HackMailApplication
 *
 * @author ace
 */
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use HackMail\Console\Command;

class HackMailApplication extends Application {
    
    /**
     * Constructor
     * 
     * @param type $version
     */
    public function __construct($version) 
    {
        parent::__construct("HackMail Console Mail Application by Amar Myana." , $version);
        
        $this->add(new Command\ShowMail());
    }
    
    /**
     * Runs the Application
     * 
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return type
     */
    public function doRun(InputInterface $input, OutputInterface $output)
    {   
        // always show the version information except when the user invokes the help
        // command as that already does it
        if ($input->hasParameterOption(array('--help', '-h')) === false 
                && $input->getFirstArgument() !==  null) {
            $output->writeln($this->getLongVersion());
            $output->writeln('');
        }
         
        return parent::doRun($input, $output);
    }
}