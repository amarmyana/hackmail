HakMail -
	HackMail (or H-Mail) is a commandline imap client written in php. It has a mysql-like interface and all mails and folder items are displayed in tables.
	It only supports gmail for now. More mail providers will be supported later.

Dependencies -
    php5
    php5-imap
    openssl
